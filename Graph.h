#pragma once
#include <iostream>
#include <string>
#include <queue>
#include <stack>
#include <list>
#include <limits.h>

using namespace std;

#ifndef Graph_tree
#define Graph_tree

class Node;
class Edge
{
	public:
		Edge(Node *org, Node *dest, int dist)
		{
			origin = org;
			destination = dest;
			distance = dist;
		}

	Node* getOrigin() { return origin; }
	Node* getDestination() { return destination; }
	int getDistance() { return distance; }
	private:
		Node * origin;
		Node *destination;
		int distance;
		};


	class Node
	{
	public:
		Node(char id)
		{
			name = id;
		}

		void addEdge(Node *v, int dist)
		{
			Edge newEdge(this, v, dist);
			edges.push_back(newEdge);
		}

		void printEdges()
		{
			cout << name << ":" << endl;
			for (list<Edge>::iterator it = edges.begin(); it != edges.end(); it++)
			{
				Edge e = *it;
				cout << e.getDestination()->getName() << " - " << e.getDistance() << endl;
			}
			cout << endl;
		}

		char getName() { return name; }
		list<Edge> getEdges() { return edges; }

	private:
		char name;
		list<Edge> edges;
	};


	class Graph
	{
	public:
		Graph() {}

		void insert(int data[4][4])
		{
			int i = 0;
			for (int i = 0; i < 4; i++)
			{
				char name = 'A' + i;
				Node v = Node(name);
				vertices.push_back(v);
			}
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				for (int j = 0; j < 4; j++)
				{
					Node &g = *it;
					char name = 'A' + j;
					Node *temp = new Node(name);
					g.addEdge(temp, data[i][j]);
				}
				i++;
			}
		}

		void printGraph()
		{
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				g.printEdges();
			}
		}
		bool CompleteGraph()
		{
			list<int> count;
			int temp = 0;
			bool check = true;
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				list<Edge> e = g.getEdges();
				for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
				{
					temp++;
				}
				count.push_back(temp);
				temp = 0;
			}
			for (list<int>::iterator num = count.begin(); num != count.end(); ++num)
			{
				if (count.begin() != num)
				{
					check = false;
				}
			}
			return check;
		}
		bool pseudograph()
		{
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				list<Edge> e = g.getEdges();
				for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
				{
					Edge &tempE = *it2;
					Node &name = *tempE.getDestination();
					if (g.getName() == name.getName() && tempE.getDistance() != 0)
					{
						return true;
					}
				}
			}
			return false;
		}

		
		

		bool WeightedGraph()
		{
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				list<Edge> e = g.getEdges();
				for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
				{
					Edge &Weight = *it2;
					if (Weight.getDistance() != 0)
					{
						return true;
					}
				}
			}
			return false;
		}

		bool digraph()
		{
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				list<Edge> e = g.getEdges();
				for (list<Node>::iterator it2 = vertices.begin(); it2 != vertices.end(); ++it2)
				{
					Node &g2 = *it2;
					list<Edge> e2 = g2.getEdges();
					for (list<Edge>::iterator edges = e.begin(); edges != e.end(); ++edges)
					{
						Edge &tempE = *edges;
						Node &name = *tempE.getDestination();
						for (list<Edge>::iterator edges2 = e2.begin(); edges2 != e2.end(); ++edges2)
						{
							Edge &tempE2 = *edges2;
							Node &name2 = *tempE2.getDestination();
							if (g.getName() != g2.getName())
							{
								if (name.getName() == g2.getName() && g.getName() == name2.getName())
								{
									return true;
								}
							}
						}
					}
				}
			}
			return false;
		}
		bool multigraph()
		{
			int count = 0;
			for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
			{
				Node &g = *it;
				list<Edge> e = g.getEdges();
				for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
				{
					Edge &tempE = *it2;
					Node &name = *tempE.getDestination();

					for (list<Edge>::iterator it3 = e.begin(); it3 != e.end(); ++it3)
					{
						Edge &tempE2 = *it3;
						Node &name2 = *tempE2.getDestination();
						if (name.getName() == name2.getName())
						{
							count++;
							if (count > 1)
							{
								return true;
							}
						}
						else
						{
							count = 0;
						}
					}
				}
			}
			return false;
		}
		int minDistance(int dist[], bool sptSet[]) // min value
		{ 
			int min = INT_MAX, min_index;

			for (int v = 0; v < 4; v++)
				if (sptSet[v] == false && dist[v] <= min)
					min = dist[v], min_index = v;

			return min_index;
		}

		void printSolution(int dist[], int n)
		{
			cout << "Vertex   Distance" << endl;
			for (int i = 0; i < n; i++)
			{
				char name = 'A' + i;
				cout << name << "           " << dist[i] << endl;
			}
		}

		void dijkstra(int src)//get start node
		{
			int dist[4]; 
			bool sptSet[4]; 
			int temp = 0;
			int First_array = 0;

			for (int i = 0; i < 4; i++) //set value
			{
				dist[i] = INT_MAX, sptSet[i] = false;
			}

			dist[src] = 0;//set start node be 0

			for (int count = 0; count < 4 - 1; count++)
			{
				int u = minDistance(dist, sptSet);//check shot part

				sptSet[u] = true;//check  node is used
				First_array = 0;
				/*Use this to choose to get distance node*/
				for (list<Node>::iterator it = vertices.begin(); First_array <= u; ++it)
				{
					if (First_array == u)
					{
						Node &g = *it;
						list<Edge> e = g.getEdges();
						for (int v = 0; v < 4; v++)
						{
							int Second_array = 0;
							for (list<Edge>::iterator it2 = e.begin(); Second_array <= v; ++it2)
							{
								Edge &tempE = *it2;
								temp = tempE.getDistance();
								Second_array++;
							}
							if (!sptSet[v] && temp && dist[u] != INT_MAX && dist[u] + temp < dist[v])
							{
								dist[v] = dist[u] + temp;
							}
						}
					}
					First_array++;
				}
			}

			printSolution(dist, 4);// distance from initial node
		}

		void printMST(int parent[])
		{
			cout << "Edge        Weight" << endl;
			for (int i = 1; i < 4; i++)
			{
				char name1 = 'A' + parent[i];
				char name2 = 'A' + i;
				cout << name1 << " - " << name2;
				int temp = 0;
				int First_array = 0;
				for (list<Node>::iterator it = vertices.begin(); First_array <= i; ++it)
				{
					if (First_array == i)
					{
						Node &g = *it;
						list<Edge> e = g.getEdges();
						int Second_array = 0;
						for (list<Edge>::iterator it2 = e.begin(); Second_array <= parent[i]; ++it2)
						{
							if (Second_array == parent[i])
							{
								Edge &tempE = *it2;
								temp = tempE.getDistance();
								cout << "        " << temp << endl;
							}
							Second_array++;
						}

					}
					First_array++;
				}
			}
		}

		void primMST()
		{
			int parent[4];
			//  store constructed MST

			int dist[4];
			bool sptSet[4];

			int temp = 0;//get data on array
			int First_array = 0;

			
			for (int i = 0; i < 4; i++) // Initiall all keys infinite
			{
				dist[i] = INT_MAX, sptSet[i] = false;
			}
			// Make key 0  
			dist[0] = 0;
			parent[0] = -1; // First node is always root  

							// The MST  have V vertices 
			for (int count = 0; count < 4 - 1; count++)
			{
				int u = minDistance(dist, sptSet);//check shot part

				sptSet[u] = true;//check node 
				First_array = 0;
				
				for (list<Node>::iterator it = vertices.begin(); First_array <= u; ++it)
				{
					if (First_array == u)
					{
						Node &g = *it;
						list<Edge> e = g.getEdges();
						for (int v = 0; v < 4; v++)
						{
							int Second_array = 0;
							for (list<Edge>::iterator it2 = e.begin(); Second_array <= v; ++it2)
							{
								Edge &tempE = *it2;
								temp = tempE.getDistance();
								Second_array++;
							}
							if (temp && sptSet[v] == false && temp < dist[v])
							{
								parent[v] = u, dist[v] = temp;
							}
						}
					}
					First_array++;
				}
			}

			 
			printMST(parent); //print
		}

	private:
		list<Node> vertices;
	};
	#endif